99x-bookstore
=============

This project has deployed in docker environment. Below instruction are assuming you have docker and docker-compose installed in your local machine.
1. go to the project folder --> cd 99x-test-bookstore

2. run command --> docker-compose up -d --build

3. run --> docker-compose exec app composer install

4. run below command if you get a permission issue --> docker-compose exec app chmod -R 777 var/logs/ var/cache/

5. run the migrations to create the tables --> docker-compose exec app bin/console doctrine:migrations:migrate

6. seed the database with initial data --> docker-compose exec app bin/console db:seed-books

7. Coupon code is "C123"

8. Use the URL: http://localhost:8080

9. To access the app with the profiler --> http://localhost:8080/app_dev.php/