#!/usr/bin/env bash

# Prepare Symfony Project
chown -R www-data:www-data var/

rm -rf var/log/* var/cache/* 
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
php bin/console cache:clear --env=prod
php bin/console cache:clear --env=env
chmod 777 -R var/cache var/logs

source /etc/apache2/envvars
exec apache2 -D FOREGROUND