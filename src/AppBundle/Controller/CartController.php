<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use AppBundle\Helper\Discount\Discount;
use AppBundle\Helper\Discount\CouponDiscount;
use AppBundle\Helper\Discount\ChildrenBookDiscount;

class CartController extends Controller
{
    private $session;

    const CHILD_CAT_NAME = 'Children';

    function __construct()
    {
        $this->session = new Session(new PhpBridgeSessionStorage());
        $this->session->start();
    }


    private function increaseCartItem($request)
    {
        try {
            $cart = [];
            if ($this->session->has('cart')) {
                $cart = $this->session->get('cart');
            }

            $key = $request->request->get('itemId');

            if (array_key_exists($key, $cart)) {
                $cart[$key] += 1;
            } else {
                $cart[$key] = 1;
            }
            
            $cartCount = 0;
            foreach ($cart as $val) {
                $cartCount += $val;
            }
            // set the data in session
            $this->session->set('cart', $cart);
            $this->session->set('cartCount', $cartCount);
        } catch (\Exception $ex) {
            throw new \Exception($ex);
        }

        return true;
    }

    /**
     * @Route("/addcart", name="add_to_cart", methods="POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function addCart(Request $request)
    {
        $this->increaseCartItem($request);

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/updatecart", name="update_cart", methods="POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function updateCart(Request $request)
    {
        $this->increaseCartItem($request);

        return $this->redirectToRoute('show_cart');
    }

    /**
     * @Route("/reducecart", name="reduce_cart", methods="POST")
     *
     * @param Request $request
     * @return void
     */
    public function reduceCart(Request $request)
    {
        $cart = [];
        if ($this->session->has('cart')) {
            $cart = $this->session->get('cart');
        }

        $key = $request->request->get('itemId');

        if (array_key_exists($key, $cart) && $cart[$key] >= 2) {
            $cart[$key] -= 1;
        } else {
            $cart[$key] = 1;
        }
        
        $cartCount = 0;
        foreach ($cart as $val) {
            $cartCount += $val;
        }
        // set the data in session
        $this->session->set('cart', $cart);
        $this->session->set('cartCount', $cartCount);

        return $this->redirectToRoute('show_cart');
    }

    /**
     * @Route("/remove-cart-item", name="remove_cart_item", methods="POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function removeCartItem(Request $request)
    {
        $cart = $this->session->get('cart');
        $key = $request->request->get('itemId');

        if (array_key_exists($key, $cart)) {
            unset($cart[$key]);
        }

        $cartCount = 0;
        foreach ($cart as $val) {
            $cartCount += $val;
        }
        // set the data in session
        $this->session->set('cart', $cart);
        $this->session->set('cartCount', $cartCount);

        return $this->redirectToRoute('show_cart');
    }

    /**
     * @Route("/update-coupon-code", name="update_coupon_code", methods="POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateCouponCode(Request $request)
    {
        $couponCode = $request->request->get('couponCode');
        if ($couponCode) {
            $this->session->set('couponCode', $couponCode, '');
        }

        return $this->redirectToRoute('show_cart');
    }

    /**
     * @Route("reset-coupon", name="reset_coupon")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resetCoupon()
    {
        $this->session->set('couponCode', '');
        return $this->redirectToRoute('show_cart');
    }

    /**
     * @Route("/cart", name="show_cart")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showCart()
    {
        $cart = $this->session->get('cart');
        $cartCount = $this->session->get('cartCount');

        $fullCart = [];

        $subTotal = 0;
        foreach ($cart as $key => $val) {
            $book = $this->getDoctrine()->getRepository(Book::class)->find($key);
            $fullCart[] = [
                'bookId' => $book->getId(),
                'bookTitle' => $book->getTitle(),
                'bookIsbn' => $book->getIsbn(),
                'bookAuthor' => $book->getAuthor(),
                'bookCategoryId' => $book->getCategoryId(),
                'bookCategory' => $book->getCategory()->getName(),
                'bookUnitPrice' => $book->getPrice(), 
                'bookQty' => $val,
                'bookTotalPrice' => number_format(($val * $book->getPrice()), 2)
            ];

            $subTotal += ($val * $book->getPrice());
        }

        // get children category to use in children category discount
        $childrenCategory = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['name' => self::CHILD_CAT_NAME]);

        // calculate any discounts
        // here we have applied the decorator design pattern to calculate the discount
        $discount = new Discount();

        $discount = new ChildrenBookDiscount($discount);
        $discount->setChildrenCategory($childrenCategory);

        // check for coupon
        $couponCode = '';
        if ($this->session->has('couponCode')) {
            $couponCode = $this->session->get('couponCode');

            $discount = new CouponDiscount($discount);
            $discount->setCouponCode($couponCode);
        }

        $discountAmount = $discount->getDiscount($fullCart);
        $discountDescription = $discount->getDescription();

        $grandTotal = $subTotal - $discountAmount;

        // adjust for minus value
        if ($grandTotal < 0) {
            $grandTotal = 0;
        }
        
        return $this->render('default/cart.html.twig', [
            'fullCart' => $fullCart,
            'subTotal' => number_format($subTotal, 2),
            'couponCode' => $couponCode,
            'discountAmount' => number_format($discountAmount, 2),
            'discountDescription' => $discountDescription,
            'grandTotal' => number_format($grandTotal, 2)
        ]);
    }
}
