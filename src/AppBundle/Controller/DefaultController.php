<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use AppBundle\Entity\Book;
use AppBundle\Entity\Category;

class DefaultController extends Controller
{
    private $session;
    
    function __construct()
    {
        $this->session = new Session(new PhpBridgeSessionStorage());
        $this->session->start();
    }

    /**
     * @Route("/", name="homepage")
     */
    public function homeAction(Request $request)
    {
        $categoryRepository = $this->getDoctrine()->getRepository(Category::class);
        $bookRepository = $this->getDoctrine()->getRepository(Book::class);

        // get all the categories
        $categories = $categoryRepository->findAll();

        $filter = '';
        if (!empty($request->query->get('category'))) {
            $filter = $request->query->get('category');

            // filter the books by category
            $books = $bookRepository->findByCategoryId($filter);
        } else {
            // get all the books
            $books = $bookRepository->findAll();
        }
        
        $cartCount = 0;
        if ($this->session->has('cartCount')) {
            $cartCount = $this->session->get('cartCount');
        }
        
        return $this->render('default/home.html.twig', [
            'categories' => $categories,
            'books' => $books,
            'filter' => $filter,
            'cartCount' => $cartCount
        ]);
    }

    

}
