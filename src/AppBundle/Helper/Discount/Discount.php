<?php

namespace AppBundle\Helper\Discount;

use AppBundle\Helper\Discount\DiscountInterface;

class Discount implements DiscountInterface
{
    const DISCOUNT = 5;
    const DISCOUNT_DESCRIPTION = '5% Discount Applied. Bought 10 Books From Each Category';

    private $discounted = false;


    /**
     * Get the total of the cart
     *
     * @param $fullCart
     * @return int
     */
    public function getTotal($fullCart)
    {
        // get the total amount
        $total = 0;
        foreach ($fullCart as $item) {
            $total += $item['bookTotalPrice'];
        }

        return $total;
    }

    /**
     * Get any previous discounts and apply the relevant discount from this class
     *
     * @param $fullCart
     * @return float|int
     */
    public function getDiscount($fullCart)
    {
        $totalDiscount = 0;

        // number of categories are fixed and hardcoded for this example
        $catCount1 = 0;
        $catCount2 = 0;

        foreach ($fullCart as $item) {
            if ($item['bookCategoryId'] == 1) {
                $catCount1 += $item['bookQty'];
            } elseif ($item['bookCategoryId'] == 2) {
                $catCount2 += $item['bookQty'];
            }
        }

        // apply discount
        if ($catCount1 >= 10 && $catCount2 >= 10) {
            $total = $this->getTotal($fullCart);

            $discountAmount = 0;
            if ($total > 0) {
                $discountAmount = ($total / 100) * self::DISCOUNT;
            }

            $this->discounted = true;
            $totalDiscount = $totalDiscount + $discountAmount;
        }

        return $totalDiscount;
    }

    /**
     * Get any previous description and append the description from this class
     *
     * @return array
     */
    public function getDescription()
    {
        $description = [];

        if ($this->discounted) {
            array_push($description, self::DISCOUNT_DESCRIPTION);
        }

        return $description;
    }
}