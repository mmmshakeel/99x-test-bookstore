<?php

namespace AppBundle\Helper\Discount;

use AppBundle\Helper\Discount\DiscountDecorator;

class CouponDiscount extends DiscountDecorator
{
    private $couponCode;
    private $discounted = false;

    const COUPON_CODE = 'C123';
    const DISCOUNT = 15;
    const DISCOUNT_DESCRIPTION = '15% Coupon Discount Applied';

    public function setCouponCode($couponCode)
    {
        $this->couponCode = $couponCode;
    }

    /**
     * Get the total of the cart
     *
     * @param $fullCart
     * @return int
     */
    public function getTotal($fullCart)
    {
        // get the total amount
        $total = 0;
        foreach ($fullCart as $item) {
            $total += $item['bookTotalPrice'];
        }

        return $total;
    }

    /**
     * Get any previous discounts and apply the relevant discount from this class
     *
     * @param $fullCart
     * @return float|int
     */
    public function getDiscount($fullCart)
    {
        $totalDiscount = $this->discount->getDiscount($fullCart);

        // clear all discounts when there is a coupon code
        if ($this->couponCode == self::COUPON_CODE) {

            // get the total amount
            $total = $this->getTotal($fullCart);

            // apply discount
            $discountAmount = 0;
            if ($total > 0) {
                $discountAmount = ($total/100) * self::DISCOUNT;
            }

            $this->discounted = true;
            $totalDiscount = $totalDiscount + $discountAmount;
        }

        return $totalDiscount;
    }

    /**
     * Get any previous description and append the description from this class
     *
     * @return array
     */
    public function getDescription()
    {
        $description = $this->discount->getDescription();

        if ($this->discounted) {
            array_push($description, self::DISCOUNT_DESCRIPTION);
        }

        return $description;
    }
}