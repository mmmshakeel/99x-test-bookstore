<?php

namespace AppBundle\Helper\Discount;
 
interface DiscountInterface
{
    public function getTotal($fullCart);

    public function getDiscount($fullCart);

    public function getDescription();
}