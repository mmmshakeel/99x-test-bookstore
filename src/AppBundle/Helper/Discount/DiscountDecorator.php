<?php

namespace AppBundle\Helper\Discount;


use AppBundle\Helper\Discount\DiscountInterface;

abstract class DiscountDecorator implements DiscountInterface
{
    protected $discount;

    public function __construct(DiscountInterface $discount)
    {
        $this->discount = $discount;
    }

    abstract public function getTotal($fullCart);

    abstract public function getDiscount($fullCart);

    abstract public function getDescription();
}