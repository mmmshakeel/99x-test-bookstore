<?php

namespace AppBundle\Helper\Discount;

use AppBundle\Helper\Discount\DiscountDecorator;

class ChildrenBookDiscount extends DiscountDecorator
{
    const DISCOUNT = 10;
    const DISCOUNT_DESCRIPTION = '10% Discount Applied. Bought 5 Books From Children Category';

    private $childrenCategory;
    private $discounted = false;

    public function setChildrenCategory($category)
    {
        $this->childrenCategory = $category;
    }

    /**
     * Get the total of the cart
     *
     * @param $fullCart
     * @return int
     */
    public function getTotal($fullCart)
    {
        // get the total amount
        $total = 0;
        foreach ($fullCart as $item) {
            $total += $item['bookTotalPrice'];
        }

        return $total;
    }

    /**
     * Get any previous discounts and apply the relevant discount from this class
     *
     * @param $fullCart
     * @return float|int
     */
    public function getDiscount($fullCart)
    {
        $totalDiscount = $this->discount->getDiscount($fullCart);

        $catCount = 0;
        foreach ($fullCart as $item) {
            if ($this->childrenCategory->getId() == $item['bookCategoryId']) {
                $catCount += $item['bookQty'];
            }
        }

        // apply discount
        if ($catCount >= 5) {
            // get total
            $total = $this->getTotal($fullCart);

            $discountAmount = 0;
            if ($total > 0) {
                $discountAmount = ($total / 100) * self::DISCOUNT;
            }

            $this->discounted = true;
            $totalDiscount = $totalDiscount + $discountAmount;
        }

        return $totalDiscount;
    }

    /**
     * Get any previous description and append the description from this class
     *
     * @return array
     */
    public function getDescription()
    {
        $description = $this->discount->getDescription();

        if ($this->discounted) {
            array_push($description, self::DISCOUNT_DESCRIPTION);
        }

        return $description;
    }
}