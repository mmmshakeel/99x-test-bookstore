<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Entity\Book;
use AppBundle\Entity\Category;

class DbSeedBooksCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('db:seed-books')
            ->setDescription('Add seed data of books to start')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $entityManager = $this->getContainer()->get('doctrine')->getManager();

        // seed categories
        $catChildren = new Category();
        $catChildren->setName('Children');
        $entityManager->persist($catChildren);
        $entityManager->flush();

        $catFiction = new Category();
        $catFiction->setName('Fiction');
        $entityManager->persist($catFiction);
        $entityManager->flush();
        

        // seed books - add 12 books to the database
        for ($i = 1; $i <= 6; $i++) {
            $book = new Book();
            $book->setTitle('Title ' . $i);
            $book->setIsbn(mt_rand(1000000000, 9999999999));
            $book->setAuthor('Author ' . $i);
            $book->setCategory($catChildren);
            $book->setPrice(mt_rand(10, 200));
            $entityManager->persist($book);
            $entityManager->flush();
        }

        for ($i = 7; $i <= 12; $i++) {
            $book = new Book();
            $book->setTitle('Title ' . $i);
            $book->setIsbn(mt_rand(1000000000, 9999999999));
            $book->setAuthor('Author ' . $i);
            $book->setCategory($catFiction);
            $book->setPrice(mt_rand(10, 200));
            $entityManager->persist($book);
            $entityManager->flush();
        }

        $output->writeln('Database seeding successful');
    }

}
